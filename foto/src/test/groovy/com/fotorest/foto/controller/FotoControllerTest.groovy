package com.fotorest.foto.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc

import com.fotorest.foto.Foto
import com.fotorest.foto.FotoRepository
import com.fotorest.foto.FotoService

import spock.lang.Specification
import spock.mock.DetachedMockFactory

@WebMvcTest
class FotoControllerTest extends Specification{
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	FotoService fotoService;
	
	@Autowired
	FotoRepository fotoRepository;
	
	@TestConfiguration
	static class MockConfig {
		def factory = new DetachedMockFactory()
		
		@Bean
		FotoService fotoService()
		{
			return factory.Mock(FotoService)
		}
		
		@Bean
		FotoRepository fotoRepository()
		{
			return factory.Mock(FotoRepository)
		}
	}
	
	@WithMockUser(username="José")
	def 'deve listar todas as fotos'()
	{
		given: 'fotos existentes na base'
		def foto = new Foto()
		def fotos = new ArrayList()
		foto.setNome('Feijao')
		fotos << foto
		
		when: 'uma consulta e feita'
		def result = mockMvc.perform(
			org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get('/'))
		
		then: 'retorna as fotos'
		1 * fotoService.buscarTodas() >> fotos
		result.andExpect(
			org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
			.andExpect(
			org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath('[0].nome').value('Feijao'))
	}
	
	@WithMockUser(username="José")
	def 'deve inserir uma nova foto'()
	{
		when: 'uma foto e inserida'
		def response = mockMvc.perform(
			org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post('/')
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content('{"nome":"Feijao"}')
				.with(
					org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf())
			)
		
		then: 'retorne a foto inserida'
		1 * fotoService.inserir(_) >> {Foto foto -> return foto}
		response.andExpect(
			org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
		.andExpect(
			org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath('$.nome').value('Feijao'))
		.andExpect(
			org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath('$.nomeUsuario').value('José'))
	}
}
