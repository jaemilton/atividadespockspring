package br.com.itau.investimentocliente.services

import com.sun.jersey.api.client.Client

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import spock.lang.Specification

class ClienteServiceTest extends Specification {
	ClienteService clienteService;
	ClienteRepository clienteRepository;
	
	def setup() 
	{
		clienteService = new ClienteService();
		clienteRepository = Mock();
		
		clienteService.clienteRepository = clienteRepository;
	}
	
	def 'deve salvar um cliente'()
	{
		given:  'os dados de um cliente são informados'
		Cliente cliente = new Cliente()
		cliente.setNome('Jaemilton')
		cliente.setCpf('301.300.908-08')	
	
		when: 'o cliente é salvo'
		def clienteSalvo = clienteService.cadastrar(cliente)
		
		then: 'retorna um cliente'
		1 * clienteRepository.save(_) >> cliente
		clienteSalvo != null
	}
	
	def 'deve buscar um cliente por CPF'()
	{
		given: 'o cliente existe na base de dados'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.setNome('Jose')
		cliente.setCpf(cpf)
		def clienteOptional = Optional.of(cliente)
		
		when: 'e feita uma busca informando o cpf'
		def clienteEncontrado = clienteService.buscar(cpf)
		
		then: 'retorno o cliente buscado'
		1 * clienteRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent() == true	
	
	}
}
