package br.com.itau.investimentocliente.controllers

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import spock.lang.Specification
import spock.mock.DetachedMockFactory

@WebMvcTest
class ClienteControllerTest extends Specification 
{
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@TestConfiguration
	static class MockConfig
	{
		def factory = new DetachedMockFactory()
		
		@Bean
		ClienteService clienteService()
		{
			return factory.Mock(ClienteService)
		}
		
		@Bean
		ClienteRepository clienteRepository()
		{
			return factory.Mock(ClienteRepository)
		}
	}
	
	def 'deve buscar um cliente por CPF'()
	{
		given: 'um cliente existente na base'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.setNome('Jaemilton')
		cliente.setCpf(cpf)
		Optional clienteOptional = Optional.of(cliente)
	
		when: 'uma busca e realizada'
		def resposta = mockMvc.perform(
			org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get('/123.123.123-12')
		)
		
		then: 'retorna um cliente'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent() == true
	}
	
	def 'deve inserir um novo cliente'()
	{
		given: 'os dados de um cliente sao informados'
		Cliente cliente = new Cliente()
		cliente.setNome('Jaemilton')
		cliente.setCpf('123.123.123-12')
		
		when: 'um novo cliente e realizado'
		def resposta = 
			mockMvc.perform(
				org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post('/')
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content('{"nome": "Jaemilton", "cpf": "123.123.123-12"}')
			)
			
		then: 'insira o cliente corretamente'
		1 * clienteService.cadastrar(_) >> cliente
		resposta.andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isCreated())
				.andExpect(jsonPath('$.nome').value('Jaemilton'))
	}
}


